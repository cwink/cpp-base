#include "base/foo.hpp"

#include <cstdlib>

auto main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) -> int {
  base::foo();

  return EXIT_SUCCESS;
}
