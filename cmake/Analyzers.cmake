find_program(CPPCHECK cppcheck)
if(CPPCHECK)
  set(CMAKE_CXX_CPPCHECK
      ${CPPCHECK}
      --suppress=missingInclude
      --enable=all
      --inline-suppr
      --inconclusive
      -i
      ${CMAKE_SOURCE_DIR}/imgui/lib)
else()
  message(SEND_ERROR "cppcheck requested but executable not found")
endif()

find_program(CLANGTIDY clang-tidy)
if(CLANGTIDY)
  set(CMAKE_CXX_CLANG_TIDY ${CLANGTIDY} -header-filter=./include/* -extra-arg=-Wno-unknown-warning-option)
else()
  message(SEND_ERROR "clang-tidy requested but executable not found")
endif()

find_program(INCLUDE_WHAT_YOU_USE include-what-you-use)
if(INCLUDE_WHAT_YOU_USE)
  set(CMAKE_CXX_INCLUDE_WHAT_YOU_USE ${INCLUDE_WHAT_YOU_USE})
else()
  message(SEND_ERROR "include-what-you-use requested but executable not found")
endif()
