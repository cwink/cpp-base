#define CATCH_CONFIG_MAIN

#include <catch2/catch.hpp>
#include <vector>

TEST_CASE("Quick check", "[main]")
{
  std::vector<double> values{ 1.0, 2.3, 6.2 };// NOLINT

  REQUIRE(values[0] == 1.0);
}
