#include "base/foo.hpp"

#include <iostream>

namespace base {
auto foo() -> void
{
  std::cout << "FOO!" << '\n';
}
}// namespace base
